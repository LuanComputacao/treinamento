<?php
include_once 'classes/Fornecedor.class.php';
include_once 'classes/Produto.class.php';

//instancia Fornecedor
$fornecedor = new Fornecedor;
$fornecedor->Codigo 		= 848;
$fornecedor->RazaoSocial 	= 'Bom Gosto Alimentos S.A';
$fornecedor->Endereco		= 'Rua Ipiranga';
$fornecedor->Cidade			= 'Po�os de Caldas';

//instancia Produto
$produto = new Produto();
$produto->Codigo		= 462;
$produto->Descricao		= 'Doce de P�ssego';
$produto->Preco			= '1.42';
$produto->Fornecedor	= $fornecedor;

//imprime atributos
echo 'Codigo do Fornecedor	:	' . $produto->Codigo . '</br>';
echo 'Descri��o				:	' . $produto->Descricao . '</br>';
echo 'C�digo				:	' . $produto->Fornecedor->Codigo . '</br>';
echo 'Raz�o Social			:	' . $produto->Fornecedor->RazaoSocial . '</br>';