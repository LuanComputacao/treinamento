<?php
	class Pessoa
	{
		var $Codigo;
		var $Nome;
		var $Altura;
		var $Idade;
		var $Nascimento;
		var $Escolaridade;
		var $Salario;

		/*Método construtor
		 *Inicia propriedades
		 */

		function __construct($Codigo, $Nome, $Altura, $Idade, $Nascimento, $Escolaridade, $Salario)
		{
			$this->Codigo = $Codigo;
			$this->Nome = $Nome;
			$this->Altura = $Altura;
			$this->Idade = $Idade;
			$this->Nascimento = $Nascimento;
			$this->Escolaridade = $Escolaridade;
			$this->Salario = $Salario;
		}



		/*Método Crescer
		 *aumenta a altura em $cntimetros
		 */
		function Crescer($centimetros)
		{
			$this->Altura+=$centimetros;
		}

		/*método Formar
		 *altera a Escolaridade para $titulacao
		 */
		function Formar($titulacao)
		{
			$this->Escolaridade = $titulacao;
		}

		/*metodo Envelhecer
		 *aumenta a Idade em $anos
		 */
		function Envelhecer($anos)
		{
			$this->idade +=$anos;
		}


		/*Método destrutor
		 *finaliza o objeto
		 *executa quando o objeto é desalocado da memória, quando atribui-se NULL, quando se utiliza unset() ou quando o programa finaliza
		 */
		function __destruct()
		{
			echo "Objeto {$this->Nome} finalizado";
		}
	}
?>