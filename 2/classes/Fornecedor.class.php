<?php
class Fornecedor
{
	var $Codigo;
	var $RazaoSocial;
	var $Endere�o;
	var $Cidade;
	var $Contato;
	
	#metodo construtor
	function __construct()
	{
		// instancia novo contato
		$this->Contato = new Contato;
	}
	
	# grava contato
	function SetContato($Nome, $Telefone, $Email)
	{
		// delega chamada de método
		$this->Contato->SetContato($Nome, $Telefone, $Email);
	}
	
	# retorna contato
	function GetContato()
	{
		return $this->Contato->GetContato();
	}
}
?>