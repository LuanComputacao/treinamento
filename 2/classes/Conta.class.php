<?php
	abstract class Conta
	{
		var $Agencia;
		var $Codigo;
		var $DataDeCriacao;
		var $Titular;
		var $Senha;
		var $Saldo;
		var $Cancelada;

		//M�todo construtor
		function __construct($Agencia, $Codigo, $DataDeCriacao, $Titular, $Senha, $Saldo )
		{
			$this->Agencia = $Agencia;
			$this->Codigo = $Codigo;
			$this->DataDeCriacao = $Titular;
			$this->Titular = $Titular;
			$this->Senha = $Senha;

			#Chamada a outro método da classe
			$this->Depositar($Saldo);
			$this->Cancelada=false;
		}


		function Retirar($quantia)
		{
			if ($quantia > 0){
				$this->Saldo-=$quantia;
			}
		}

		function Depositar($quantia)
		{
			if ($quantia > 0) {
				$this->Saldo+=$quantia;
			}
		}

		function ObterSaldo()
		{
			return $this->Saldo;
		}
		
		abstract function Tranferir($Conta, $Valor);

		//Metodo destrutor
		function __destruct()
		{
			echo "Objeto Conta {$this->Codigo} de {$this->Titular->Nome} finalizada...</br>";
		}
	}
?>