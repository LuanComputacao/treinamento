<?php
	final class ContaPoupanca extends Conta
	{
		var $Aniversario;
		
		/*m�todo construtor (sobrescrito)
		 * agora, tamb�m inicializa a vari�vel $Aniversario
		 */
		function __construct($Agencia, $Codigo, $DataDeCriacao, $Titular, $Senha, $Saldo, $Aniversario)
		{
			//chamada do m�todo cosntrutor da classe-pai
			parent::__construct($Agencia, $Codigo, $DataDeCriacao, $Titular, $Senha, $Saldo);
			$this->Aniversario;
		}
		
		/*m�todo Retirar(sobrescrito)
		 * verifica se h� saldo para retirar tal $quantia.
		 */
		function Retirar($quantia)
		{
			if ($this->Saldo>=$quantia) {
				//Executa o metodo da classe pai.
				parent::Retirar($quantia);
			} else {
				echo "Retirada n�o permitida";
				return false;
			}
			
			//retirada permitida
			return true;
		}
		
		function Tranferir($Conta, $Valor)
		{
			if ($this->Retirar($Valor)) {
				$Conta->Depositar($Valor);
			}
		}
	}
?>